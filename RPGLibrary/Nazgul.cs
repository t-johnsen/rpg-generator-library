﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLiberary
{
    public class Nazgul : Creature
    {
        public Nazgul(int characterID, string name, int hp, int energy, int armor, double hight) : base(characterID, name, hp, energy, armor, hight)
        {
        }

        public override void Attack()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, deals 110 attack points to opponents.");
        }

        public override void Move()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, moves a huge amount of distance.");
        }

        public override void Scream()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, screams so loud that opponants get disabled for a short period of time.");

        }
    }
}
