﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLiberary
{
    public class Creature : Character, IScream
    {
        public Creature(int characterID, string name, int hp, int energy, int armor, double hight) : base(characterID, name, hp, energy, armor, hight)
        {
        }

        public virtual void Scream()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, screams at opponents.");

        }
    }
}
