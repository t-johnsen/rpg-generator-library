﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLiberary
{
    public class Elf : Character, IHeal
    {
        public Elf(int characterID, string name, int hp, int energy, int armor, double hight) : base(characterID, name, hp, energy, armor, hight)
        {
        }

        public override void Attack()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, deals 70 attack points to opponents.");
        }

        public void HealOther()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, heals other character.");
        }

        public void HealSelf()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, heals itself.");
        }

        public override void Move()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, moves a great amount of distance.");

        }
    }
}
