﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLiberary
{
    public class Man : Character
    {
        public Man(int characterID, string name, int hp, int energy, int armor, double hight) : base(characterID, name, hp, energy, armor, hight)
        {
        }

        public override void Attack()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, deals 90 attack points to opponents.");
        }

        public override void Move()
        {
            base.Move();
        }
    }
}
