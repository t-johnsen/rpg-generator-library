﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLiberary
{
    interface IHeal
    {
        public void HealOther();
        public void HealSelf();
    }
}
