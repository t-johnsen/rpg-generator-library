﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLiberary
{

    public enum ClassType { Elf, Man, Urukhai, Nazgul }
    public class Character
    {

        public int CharacterID { get; set; }
        public string Name { get; set; }
        public int HP { get; set; }
        public int Energy { get; set; }
        public int Armor { get; set; }
        public double Hight { get; set; }

        public Character(int characterID, string name, int hp, int energy, int armor, double hight)
        {
            CharacterID = characterID;
            Name = name;
            HP = hp;
            Energy = energy;
            Armor = armor;
            Hight = hight;
        }

        public virtual void Move()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, moves a standard amount of distance.");
        }

        public virtual void Attack()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, deals 40 attack points to opponents.");

        }

        public override string ToString()
        {
            return Name;
        }
    }
}
