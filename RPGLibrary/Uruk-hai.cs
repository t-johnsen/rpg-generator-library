﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLiberary
{
    public class Uruk_hai : Creature
    {
        public Uruk_hai(int characterID, string name, int hp, int energy, int armor, double hight) : base(characterID, name, hp, energy, armor, hight)
        {
        }

        public override void Attack()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, deals 100 attack points to opponents.");
        }

        public override void Move()
        {
            base.Move();
        }

        public override void Scream()
        {
            Console.WriteLine($"{Name}, the {this.GetType().Name}, screms, but has no other effect than spitting in opponents face.");

        }
    }
}
